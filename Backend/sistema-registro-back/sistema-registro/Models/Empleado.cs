﻿namespace sistema_registro.Models
{
    public class Empleado
    {
        public int Id { get; set; }
        public string primerApellido { get; set; }
        public string segundoApellido { get; set; }
        public string primerNombre { get; set; }
        public string segundoNombre { get; set; }
        public string paisEmpleo{ get; set; }
        public string tipoIdentificacion { get; set; }
        public string numeroDocumento { get; set; }
        public string correoElectonico { get; set; }

        public DateTime fechaIngreso { get; set; }
        public string area { get; set; }
        public string? estado { get; set; }
        public DateTime? fechaHoraRegistro { get; set; }
        public DateTime? fechaActualizacion { get; set; }
    }
}
