﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using sistema_registro.Models;

namespace sistema_registro.Data
{
    public class sistema_registroContext : DbContext
    {
        public sistema_registroContext (DbContextOptions<sistema_registroContext> options)
            : base(options)
        {
        }

        public DbSet<sistema_registro.Models.Empleado> EmpleadoM { get; set; } = default!;
    }
}
