﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using sistema_registro.Data;
using sistema_registro.Models;

namespace sistema_registro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadoesController : ControllerBase
    {
        private readonly sistema_registroContext _context;

        public EmpleadoesController(sistema_registroContext context)
        {
            _context = context;
        }

        // GET: api/Empleadoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Empleado>>> GetEmpleadoM()
        {
          if (_context.EmpleadoM == null)
          {
              return NotFound();
          }
            return await _context.EmpleadoM.ToListAsync();
        }

        // GET: api/Empleadoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Empleado>> GetEmpleado(int id)
        {
          if (_context.EmpleadoM == null)
          {
              return NotFound();
          }
            var empleado = await _context.EmpleadoM.FindAsync(id);

            if (empleado == null)
            {
                return NotFound();
            }

            return empleado;
        }

        // PUT: api/Empleadoes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpleado(int id, Empleado empleado)
        {
            if (id != empleado.Id)
            {
                return BadRequest();
            }
            empleado.fechaActualizacion = DateTime.Now;

            _context.Entry(empleado).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpleadoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Empleadoes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Empleado>> PostEmpleado(Empleado empleado)
        {
            try
            {
                if (_context.EmpleadoM == null)
                {
                    return Problem("Entity set 'sistema_registroContext.EmpleadoM'  is null.");
                }
                empleado.fechaHoraRegistro = DateTime.Now;
                empleado.estado = "ACTIVO";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            _context.EmpleadoM.Add(empleado);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEmpleado", new { id = empleado.Id }, empleado);
        }

        // DELETE: api/Empleadoes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmpleado(int id)
        {
            if (_context.EmpleadoM == null)
            {
                return NotFound();
            }
            var empleado = await _context.EmpleadoM.FindAsync(id);
            if (empleado == null)
            {
                return NotFound();
            }

            _context.EmpleadoM.Remove(empleado);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool EmpleadoExists(int id)
        {
            return (_context.EmpleadoM?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
