import { Component, OnInit } from '@angular/core';
import { ISEmpleado } from 'src/app/servicios/empleados.service';
import { PrimeNGConfig } from 'primeng/api';
import { CrudApiService } from 'src/app/servicios/servicios-api.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.scss']
})
export class EmpleadosComponent implements OnInit {

  empleados: ISEmpleado[] = [];

  inputEmpleados: ISEmpleado = {
    primerApellido: '',
    segundoApellido: '',
    primerNombre: '',
    segundoNombre: '',
    paisEmpleo: '',
    tipoIdentificacion: '',
    numeroDocumento: '',
    correoElectonico: '',
    fechaIngreso: '',
    area: '',
    estado: '',
    fechaHoraRegistro: '',
    fechaActualizacion: ''
  };

  loading: boolean = false;

  displayModal: boolean = false;

  selectedTipoDocument: any;
  selectedPais: any;
  selectedArea: any;
  area = [
    { codigo: "admin", name: "Administración" },
    { codigo: "fin", name: "Financiera" },
    { codigo: "comp", name: "Compras" },
    { codigo: "inf", name: "Infraestructura" }
  ]

  tiposDocument = [
    { codigo: "CC", name: "Cedula de ciudadania" },
    { codigo: "TI", name: "Tarjeta de identidad" },
    { codigo: "CE", name: "Cédula de Extranjería" },
    { codigo: "PS", name: "Pasaporte" },
    { codigo: "PE", name: "Permiso Especial" },
  ];
  pais = [
    { codigo: "COL", name: "Colombia" },
    { codigo: "EEUU", name: "Estados Unidos" }];

  b_prim_apellido: boolean = false;
  b_prim_nombre: boolean = false;
  b_seg_nombre: boolean = false;
  b_num_documento: boolean = false;
  b_seg_apellido: boolean = false;

  constructor(private primengConfig: PrimeNGConfig, private api: CrudApiService) {

  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.getEmpleados();
  }
  showModalDialog() {
    this.displayModal = true;
  }

  async getEmpleados() {
    await this.api.getData('Empleadoes').subscribe(data => {
      console.log('Empleados: -----> ', data);
      this.empleados = data;
    }, err => {
      console.log(err);
    });
  }


  validationInput() {
    this.b_prim_apellido = false;
    this.b_prim_nombre = false;
    this.b_seg_nombre = false;
    this.b_seg_apellido = false;

    const inputPrimApellido: any = document.getElementById('primApellido');
    const inputPrimNombre: any = document.getElementById('primNombre');
    const inputSegNombre: any = document.getElementById('segNombre');
    const inputNumDocumento: any = document.getElementById('numDocumento');
    const inputSegApellido: any = document.getElementById('segApellido');

    if (!inputPrimApellido.checkValidity()) {
      this.b_prim_apellido = true;
    }
    if (!inputPrimNombre.checkValidity()) {
      this.b_prim_nombre = true;
    }
    if (!inputSegNombre.checkValidity()) {
      this.b_seg_nombre = true;
    }
    if (!inputNumDocumento.checkValidity()) {
      this.b_num_documento = true;
    }
    if (!inputSegApellido.checkValidity()) {
      this.b_seg_apellido = true;
    }
    const dominio = '@cidenet.com.co';
    this.inputEmpleados.tipoIdentificacion = this.selectedTipoDocument.name;
    this.inputEmpleados.paisEmpleo = this.selectedPais.name;
    this.inputEmpleados.area = this.selectedArea.name;
    this.inputEmpleados.correoElectonico = this.inputEmpleados.primerNombre + '.' + this.inputEmpleados.primerApellido + dominio;
    this.inputEmpleados.fechaHoraRegistro = '2022-08-19T05:00:00.000Z';
    this.inputEmpleados.fechaActualizacion = '2022-08-19T05:00:00.000Z';
  }

  async postEmpleados() {
    console.log(this.inputEmpleados)
    await this.api.postData('Empleadoes', this.inputEmpleados).subscribe(data => {
      console.log('Empleados: -----> ', data);
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Empleado agregado.',
        showConfirmButton: false,
        timer: 1500
      })
      this.getEmpleados();
    }, err => {
      console.log(err);
    });

    this.inputEmpleados = {
      primerApellido: '',
      segundoApellido: '',
      primerNombre: '',
      segundoNombre: '',
      paisEmpleo: '',
      tipoIdentificacion: '',
      numeroDocumento: '',
      correoElectonico: '',
      fechaIngreso: '',
      area: '',
      estado: '',
      fechaHoraRegistro: '',
      fechaActualizacion: ''
    };
  }
}
