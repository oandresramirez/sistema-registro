import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CrudApiService {
  host = 'https://localhost:7095/api/';
  constructor(private http: HttpClient) { }

  getData(param: string): Observable<any> {
    const service = this.host + param;
    return this.http.get(service).pipe(map(response => response));
  }
  postData(param: string, data: any): Observable<any> {
    const service = this.host + param;
    return this.http.post(service, data).pipe(map(response => response));
  }
  postDataLogin(param: string, data: any): Observable<any> {
    const service = this.host + param;
    return this.http.post(service, data).pipe(map(response => response));
  }
  putData(param: string, data: any): Observable<any> {
    const service = this.host + param;
    return this.http.put(service, data).pipe(map(response => response));
  }
  deleteData(param: string): Observable<any> {
    const service = this.host + param;
    return this.http.delete(service).pipe(map(response => response));
  }



}