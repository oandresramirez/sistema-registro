export interface ISEmpleado{
    id?: number;
    primerApellido?: string;
    segundoApellido?: string;
    primerNombre?: string;
    segundoNombre?: string;
    paisEmpleo?: string;
    tipoIdentificacion?: string;
    numeroDocumento?: string;
    correoElectonico?: string;
    fechaIngreso?: string;
    area?: string;
    estado?: string;
    fechaHoraRegistro?: string;
    fechaActualizacion?: string;
}